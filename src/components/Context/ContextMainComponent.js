import React, { Component } from 'react'
import { UserConsumer } from './UserContext'

export class ContextMainComponent extends Component {
	render() {
		return (
			<UserConsumer>
				{username => {
					return <div>Context Example <br/>Hello {username} Company</div>
				}}
			</UserConsumer>
		)
	}
}

export default ContextMainComponent
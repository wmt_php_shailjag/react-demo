import React, { Component } from 'react'
import ContextMainComponent from './ContextMainComponent';

export class ComponentMain extends Component {
	render() {
		return (
			<div>
				<ContextMainComponent />
			</div>
		)
	}
}

export default ComponentMain
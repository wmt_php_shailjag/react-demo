import React, { Component } from 'react'
import ComponentMain from './ComponentMain'

export class ComponentMainA extends Component {
	render() {
		return (
			<div>
				<ComponentMain />
			</div>
		)
	}
}

export default ComponentMainA
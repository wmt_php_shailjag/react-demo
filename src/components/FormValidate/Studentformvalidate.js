import React, { Component } from "react";
import "./Formvalidatecss.css";

const school = [
  {
    label: "--Select--",
    value: "--select--",
  },
  {
    label: "SNK school",
    value: "SNK school",
  },
  {
    label: "VJM school",
    value: "VJM school",
  },
  {
    label: "DPS school",
    value: "DPS school",
  },
  {
    label: "RKC school",
    value: "RKC school",
  },
];
const gender = [
  {
    label: "--Select--",
    value: "--select--",
  },
  {
    label: "male",
    value: "male",
  },
  {
    label: "female",
    value: "female",
  },
  {
    label: "other",
    value: "other",
  },
];
const city = [
  {
    label: "--Select--",
    value: "--select--",
  },
  {
    label: "Rajkot",
    value: "Rajkot",
  },
  {
    label: "Ahemdabad",
    value: "Ahemdabad",
  },
];
const hobby = [
  {
    label: "--Select--",
    value: "--select--",
  },
  {
    label: "Drawing",
    value: "Drawing",
  },
  {
    label: "Singing",
    value: "Singing",
  },
  {
    label: "Dancing",
    value: "Dancing",
  },
  {
    label: "Photography",
    value: "Photography",
  },
  {
    label: "Traveling",
    value: "Traveling",
  },
];

class Studentformvalidate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studName: "",
      emailId: "",
      dob: "",
      school: "select",
      gender: "select",
      phoneNumber: "",
      address: "",
      city: "select",
      hobby: "select",

      formErrors: {},
    };

    this.initialState = this.state;
  }

  handleFormValidation = () => {
    const { studName, emailId, phoneNumber } = this.state;
    let formErrors = {};
    let formIsValid = true;

    switch (true) {
      case this.state.studName.length < 5 || /[^a-zA-Z\s]/.test(studName):
        formIsValid = false;
        formErrors["studNameErr"] = "Enter full name and Allow only letters";
        break;
      case !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(emailId):
        formIsValid = false;
        formErrors["emailIdErr"] = "Invalid email id.";
        break;
      case !/^([0]|\+91)?[789]\d{9}$/.test(phoneNumber):
        formIsValid = false;
        formErrors["phoneNumberErr"] = "Invalid phone number. Use 0 or +91";
        break;
      case this.state.gender === "select":
        formIsValid = false;
        formErrors["genderErr"] = "Select gender.";
        break;
      case this.state.hobby === "select":
        formIsValid = false;
        formErrors["hobbyErr"] = "Select hobby.";
        break;
      case this.state.city === "select":
        formIsValid = false;
        formErrors["cityErr"] = "Select city.";
        break;
      case this.state.school === "select":
        formIsValid = false;
        formErrors["schoolErr"] = "Select School.";
        break;
      default:
        break;
    }

    this.setState({ formErrors: formErrors });
    return formIsValid;
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
   if(this.handleFormValidation()){
    this.setState.formErrors={};
   }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.handleFormValidation()) {
      alert(
        "You have been successfully registered." +
          "\n Name: " +
          this.state.studName +
          "\n emailId: " +
          this.state.emailId +
          "\n Birth date: " +
          this.state.dob +
          "\n gender: " +
          this.state.gender +
          "\n phoneNumber: " +
          this.state.phoneNumber +
          "\n school: " +
          this.state.school +
          "\n Hobby: " +
          this.state.hobby +
          "\n address: " +
          this.state.address+
          "\n city: " +
          this.state.city 
      );

      this.setState(this.initialState);
    }
  };
  render() {
    const {
      studNameErr,
      emailIdErr,
      dobErr,
      genderErr,
      phoneNumberErr,
      cityErr,
      schoolErr,
      hobbyErr,
      addressErr,
    } = this.state.formErrors;

    return (
      <div className="formDiv">
        <div className="card">
          <div
            style={{
              textAlign: "center",
              fontSize: "50px",
              fontWeight: "bolder",
              margin: "15px",
            }}
          >
            Student Details
          </div>
          <div style={{ textAlign: "center" }}>All fields are required</div>
          <form onSubmit={this.handleSubmit} method="POST" action="#">
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <div
                style={{ display: "flex", flexFlow: "column", width: "40%" }}
              >
                {/*Name*/}
                <div className="lbl-space">
                  <div>
                    <label>Full Name</label>
                  </div>
                  <div>
                    <input
                      type="text"
                      name="studName"
                      required
                      value={this.state.studName}
                      onChange={this.handleChange}
                      placeholder="Your full name.."
                      className={studNameErr ? " showError" : ""}
                    />
                    {studNameErr && (
                      <div style={{ color: "red" }}>{studNameErr}</div>
                    )}
                  </div>
                </div>

                {/*Emailid*/}
                <div className="lbl-space">
                  <div>
                    <label>Email Id</label>
                  </div>
                  <div>
                    <input
                      type="text"
                      name="emailId"
                      required
                      value={this.state.emailId}
                      onChange={this.handleChange}
                      placeholder="Your email id.."
                      className={emailIdErr ? " showError" : ""}
                    />
                    {emailIdErr && (
                      <div style={{ color: "red" }}>{emailIdErr}</div>
                    )}
                  </div>
                </div>

                {/*School*/}
                <div className="lbl-space">
                  <div>
                    <label>School Name</label>
                  </div>
                  <div>
                    <select
                      name="school"
                      required
                      onChange={this.handleChange}
                      className={schoolErr ? " showError" : ""}
                      value={this.state.school}
                    >
                      {school.map((school) => (
                        <option value={school.value}>{school.label}</option>
                      ))}
                    </select>

                    {schoolErr && (
                      <div style={{ color: "red" }}>{schoolErr}</div>
                    )}
                  </div>
                </div>

                {/*Birthdate*/}
                <div className="lbl-space">
                  <div>
                    <label>Birth Date</label>
                  </div>
                  <div>
                    <input
                      type="text"
                      name="dob"
                      required
                      value={this.state.dob}
                      onChange={this.handleChange}
                      placeholder="DD/MM/YYYY.."
                      className={dobErr ? " showError" : ""}
                    />
                    {dobErr && <div style={{ color: "red" }}>{dobErr}</div>}
                  </div>
                </div>

                {/*Gender*/}
                <div className="lbl-space">
                  <div>
                    <label>Gender</label>
                  </div>
                  <div>
                    <select
                      name="gender"
                      required
                      onChange={this.handleChange}
                      className={genderErr ? " showError" : ""}
                      value={this.state.gender}
                    >
                      {gender.map((gender) => (
                        <option value={gender.value}>{gender.label}</option>
                      ))}
                    </select>
                    {genderErr && (
                      <div style={{ color: "red" }}>{genderErr}</div>
                    )}
                  </div>
                </div>
              </div>

              <div
                style={{ display: "flex", flexFlow: "column", width: "40%" }}
              >
                {/*phone number*/}
                <div className="lbl-space">
                  <div>
                    <label>Phone Number</label>
                  </div>
                  <div>
                    <input
                      type="text"
                      required
                      name="phoneNumber"
                      onChange={this.handleChange}
                      value={this.state.phoneNumber}
                      placeholder="Start with 0 or +91"
                      className={phoneNumberErr ? " showError" : ""}
                    />
                    {phoneNumberErr && (
                      <div style={{ color: "red" }}>{phoneNumberErr}</div>
                    )}
                  </div>
                </div>

                {/*Address*/}
                <div className="lbl-space">
                  <div>
                    <label>Address</label>
                  </div>
                  <div>
                    <textarea
                      rows={7}
                      name="address"
                      required
                      onChange={this.handleChange}
                      value={this.state.address}
                      placeholder="Write full address"
                      className={addressErr ? " showError" : ""}
                    />
                    {addressErr && (
                      <div style={{ color: "red" }}>{addressErr}</div>
                    )}
                  </div>
                </div>

                {/*City*/}
                <div className="lbl-space">
                  <div>
                    <label>City</label>
                  </div>
                  <div>
                    <select
                      name="city"
                      required
                      value={this.state.city}
                      onChange={this.handleChange}
                      className={cityErr ? " showError" : ""}
                    >
                      {city.map((city) => (
                        <option value={city.value}>{city.label}</option>
                      ))}
                    </select>
                    {cityErr && <div style={{ color: "red" }}>{cityErr}</div>}
                  </div>
                </div>

                {/*hobby*/}
                <div className="lbl-space">
                  <div>
                    <label htmlFor="hobby">Hobby</label>
                  </div>
                  <div>
                    <select
                      name="hobby"
                      value={this.state.hobby}
                      required
                      onChange={this.handleChange}
                      className={hobbyErr ? " showError" : ""}
                    >
                      {hobby.map((hobby) => (
                        <option value={hobby.value}>{hobby.label}</option>
                      ))}
                    </select>
                    {hobbyErr && <div style={{ color: "red" }}>{hobbyErr}</div>}
                  </div>
                </div>
              </div>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <input type="submit" value="Submit" />
              <input
                type="reset"
                value="Reset"
                onClick={() => {
                  this.setState(this.initialState);
                }}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Studentformvalidate;

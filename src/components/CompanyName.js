import React from 'react'

function CompanyName ({ companyName }) {
  if (companyName === 'abc') {
    throw new Error('Not in our company!')
  }
  return <h3>{companyName} Error Boundary Example</h3>
}

export default CompanyName
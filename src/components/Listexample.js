function Listexample() {
    const company = {
      name: 'Chris',
      twitter: 'chris__sev',
      bio: 'The dude'
    };

  
    return (
      <div>
         <h3> List and Key Example</h3>
        {Object.keys(company).map(key => (
          <p>
            <strong>{key.toUpperCase()}: </strong>
            {company[key]}
          </p>
        ))}
      </div>
    );

        

  }
  export default Listexample;
import React from 'react';

class Formexample extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
         data: 'Initial data...'
      }
      this.updateState = this.updateState.bind(this);
   };
   updateState(event) {
      this.setState({data: event.target.value});
   }
   render() {
      return (
         <div>
             <h3>Form Example</h3>
            <input type = "text" value = {this.state.data} 
               onChange = {this.updateState} />
            <h4>{this.state.data}</h4>
         </div>
      );
   }
}
export default Formexample;
import React from "react";

// Simple rendering with short syntax
class FragmentDemo extends React.Component {
render() {
	return (
	<>{/*<React.Fragment>*/}
		<h2>Fragment</h2>

		<p>This is simple Fragment Demo</p>
	</>/*</React.Fragment>*/
	);
}
}

export default FragmentDemo;


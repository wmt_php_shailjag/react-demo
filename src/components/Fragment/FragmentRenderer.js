import React from 'react';

const items = ["Item 1", "Item 2", "Item 3"];

const FragmentRenderer = () => (
  <React.Fragment>
    <h1>List of all items:</h1>
    {items.map((item, index) => (
      <React.Fragment key={index}>
        <p>Rendering Item:</p>
        <p>{item}</p>
      </React.Fragment>
    ))}
  </React.Fragment>
);
export default FragmentRenderer;
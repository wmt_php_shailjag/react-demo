import React, { Component } from 'react'

class Btnclick extends Component {
constructor(props){
	super(props)
	
	// Set initial state
	this.state = {msg : 'Hi, There!'}
	
	// Binding this keyword
	this.handleClick = this.handleClick.bind(this)
}

handleClick(){
	
	// Changing state
	this.setState({msg : 'Welcome to the React world!'})
}
	
render(){
	return (
	<div>
		<h3>Handle Event and State :</h3>
		<p>{this.state.msg}</p>

		
		
		<button onClick={this.handleClick}>
		Click here!
		</button>
	</div>
	)
}
}

export default  Btnclick ;


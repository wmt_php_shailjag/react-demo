import React, { Component } from 'react'

class Lifecycle extends Component {
	constructor(props) {
		super(props)
		this.state = {
			name: 'Webmob'
		}
		console.log('Lifecycle constructor Method()')
	}

	static getDerivedStateFromProps(props, state) {
		console.log('Lifecycle getDerivedStateFromProps Method()')
		return null
	}

	componentDidMount() {
		console.log('Lifecycle componentDidMount Method()')
	}

	shouldComponentUpdate() {
		console.log('Lifecycle shouldComponentUpdate Method()')
		return true
	}

	getSnapshotBeforeUpdate(prevProps, prevState) {
		console.log('Lifecycle getSnapshotBeforeUpdate Method()')
    return null
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('Lifecycle componentDidUpdate Method()')
	}

	changeState = () => {
		this.setState({
			name: 'WEBMOB Solution'
		})
	}

	render() {
		console.log('Lifecycle render Method()')
		return (
			<div >
				<h3>{this.state.name}</h3>
				<button onClick={this.changeState}>Change state</button>
				
			</div>
		)
	}
}

export default Lifecycle
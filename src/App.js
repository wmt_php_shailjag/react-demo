import React from "react";
import "./App.css";

/*import Renderhtml from "./components/Renderhtml";
import Btnclick from "./components/Btnclick";
import Conditionalcheck from "./components/Conditionalcheck";
import Listexample from "./components/Listexample";
import Formexample from "./components/Formexample";
import Liftingstateup from "./components/Liftingstateup";
import Lifecycle from "./components/Lifecycle";
import ClickEvent from "./components/ClickEvent";
import Studentformvalidate from "./components/FormValidate/Studentformvalidate";*/
import FragmentDemo from "./components/Fragment/FragmentDemo";
import FragmentRenderer from "./components/Fragment/FragmentRenderer";
import ErrorBoundary from "./components/ErrorBoundary";
import ComponentMain from "./components/Context/ComponentMain";
import CompanyName from "./components/CompanyName";

const App = () => {
  // Using useState hooks for defining state
  /*const [counter1, setCounter1] = useState(0);
  const increase1 = () => {
    setCounter1(counter1 + 1);
  };*/
  return (
    <div>
      {/*<h1>Hello, world!</h1>
      demo()
      <Renderhtml />
      <Btnclick />
      <Conditionalcheck />
      <Listexample />
      <Formexample />
     <Liftingstateup />
      <ClickEvent value={counter1} onClick={increase1} />
      <Lifecycle />
      <Studentformvalidate />*/}
      <FragmentDemo />
      <FragmentRenderer />
     {/*code to throw error
       <ErrorBoundary >
      <CompanyName companyName="abc" />
    </ErrorBoundary>*/}
      {/*Without Error */}
      <CompanyName companyName="webmob" />
      <ComponentMain />
    </div>
  );
};
/*const demo = () => {
  let username = "Username";
  return <div className="greeting">{username} </div>;
}*/

export default App;
